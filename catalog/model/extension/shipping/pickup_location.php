<?php
class ModelExtensionShippingPickupLocation extends Model {
	function getQuote($address) {
		$this->load->language('extension/shipping/pickup_location');

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('shipping_pickup_location_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");

		if (!$this->config->get('shipping_pickup_location_geo_zone_id')) {
			$status = true;
		} elseif ($query->num_rows) {
			$status = true;
		} else {
			$status = false;
		}

		$method_data = array();

		if ($status) {
			$quote_data = array();

			$this->load->model('localisation/location');

			$locations = $this->getLocations();

			foreach ($locations as $location) {
				$quote_data['pickup_location_'.$location['location_id']] = array(
					'code'         => 'pickup_location.pickup_location_'.$location['location_id'],
					'title'        => $this->language->get('text_description') . ' ' . $location['name'],
					'cost'         => 0.00,
					'tax_class_id' => 0,
					'text'         => $this->currency->format(0.00, $this->session->data['currency'])
				);
			}

			$method_data = array(
				'code'       => 'pickup_location',
				'title'      => $this->language->get('text_title'),
				'quote'      => $quote_data,
				'sort_order' => $this->config->get('shipping_pickup_location_sort_order'),
				'error'      => false
			);
		}

		return $method_data;
	}

	public function getLocations() {
		$query = $this->db->query("SELECT location_id, name, address, geocode, telephone, fax, image, open, comment FROM " . DB_PREFIX . "location");

		return $query->rows;
	}
}